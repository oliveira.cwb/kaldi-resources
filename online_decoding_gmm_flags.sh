#!/bin/bash
# online_decoding_gmm.sh \
#   --test-mode simulated \
#   --ac-model-dir /home/larissa/fb-gitlab/fb-asr/fb-asr-resources/am/tri3_8k_16 \
#   --audio-dir    /home/larissa/fb-gitlab/fb-asr/fb-asr-resources/audio \
#   --output_dir ./online_decoding_gmm

# Set the paths to the binaries and scripts needed
KALDI_ROOT=`pwd`/../..
export PATH=$PWD/utils/:$KALDI_ROOT/src/onlinebin:$KALDI_ROOT/src/online2bin:$KALDI_ROOT/src/bin:$KALDI_ROOT/tools/portaudio:$PATH

output_dir=./online_decoding_gmm #default

function usage() {
    echo "Usage: $0 [opts] <flag> <arguments>"
    echo "  e.g.: (bash) $0"
    echo "    --test-mode simulated"
    echo "    --am-dir    ${HOME}/fb-gitlab/fb-asr/fb-asr-resources/am/tri3_8k_16"
    echo "    --audio-dir ${HOME}/fb-asr/fb-asr-resources/kaldi-resources/audio"
    echo 
    echo "arguments:"
    echo "    --test-mode     either \"live\" or \"simulated\""
    echo "    --ac-model-dir  acustic model directory"
    echo "    --audio-dir     audio data directory"
    echo "Optional:"
    echo "    --output-dir    specify a directory to store output in"
    exit 1
}

if test $# -eq 0 ; then
    usage
fi

while [[ $# -gt 0 ]]
do
    key="$1"
    case $key in
        --test-mode)
            test_mode="$2"
            shift # past argument
            shift # past value
        ;;
        --ac-model-dir)
            ac_model_dir="$2"
            shift # past argument
            shift # past value
        ;;
        --audio-dir)
        audio_dir="$2"
            shift # past argument
            shift # past value
            ;;
        --output_dir)
        output_dir="$2"
            shift # past argument
            shift # past value
            ;;
        *)    # unknown option
    esac
done

if [[ -z $test_mode || -z $ac_model_dir || -z $audio_dir ]] ; then
    echo "Missing Arguments"
    echo 
    usage
    exit 1
fi

trans_matrix=""
if [ -s $ac_model_dir/final.mat ]; then
    trans_matrix=$ac_model_dir/final.mat
fi

if [ $test_mode == "live" ]; then
    online-gmm-decode-faster \
        --rt-min=0.5 \
        --rt-max=0.7 \
        --max-active=4000 \
        --beam=12.0 \
        --acoustic-scale=0.0769 \
        $ac_model_dir/final.mdl $ac_model_dir/graph/HCLG.fst \
        $ac_model_dir/graph/words.txt '1:2:3:4:5' $trans_matrix
elif [ $test_mode == "simulated" ]; then
        mkdir -p $output_dir
        # make an input .scp file
        > $output_dir/input.scp
        for f in $audio_dir/*.wav; do
            bf=`basename $f`
            bf=${bf%.wav}
            echo $bf $f >> $output_dir/input.scp
        done
        online-wav-gmm-decode-faster \
            --verbose=1 \
            --rt-min=0.8 \
            --rt-max=0.85 \
            --max-active=4000 \
            --beam=12.0 \
            --acoustic-scale=0.0769 \
            scp:$output_dir/input.scp $ac_model_dir/final.mdl $ac_model_dir/graph/HCLG.fst \
            $ac_model_dir/graph/words.txt '1:2:3:4:5' ark,t:$output_dir/trans.txt \
            ark,t:$output_dir/ali.txt $trans_matrix | tee -a $output_dir/transcriptions.tmp
else
        echo "Invalid test mode! Should be either \"live\" or \"simulated\"!"
fi

# Saving transcriptions to a file
sed '/^[[:space:]]*$/d' $output_dir/transcriptions.tmp > $output_dir/transcript.tmp
awk -F" " '{print $1}' $output_dir/input.scp  >> $output_dir/filename.tmp
paste $output_dir/filename.tmp $output_dir/transcript.tmp > $output_dir/transcriptions.txt

rm $output_dir/*.tmp
