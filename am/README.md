# Acoustic Models for Brazilian Portuguese Using Kaldi Tools

**[Paper](https://www.isca-speech.org/archive/IberSPEECH_2018/abstracts/IberS18_P1-13_Batista.html): Baseline Acoustic Models for Brazilian Portuguese Using Kaldi Tools**    
A comparison between Kaldi and CMU Sphinx for Brazilian Portuguese was
performed. Resources for both toolkits were developed and made publicly 
available to the community.     

For details on how to train an acoustic model using Kaldi tools, please refer to
this [this link](https://gitlab.com/fb-asr/fb-am-tutorial/kaldi-am-train).

* __am/tri3_8k_16__:      
    * **final.mdl**: the acoustic model   
    * **final.mat**: feature LDA matrix   
    * **HCLG.fst**: the complete FST   
    * **phones.txt**: a list of phoneme boundary information required for word alignemnt 
    * **words.txt**: word dictionary (mapping word ids to their textual representation)      
* __am/nnet2__: coming soon     


## Citation

If you use these resources or want to mention the paper referred above, please 
cite us as one of the following: 

> Batista, C., Dias, A.L., Sampaio Neto, N. (2018) Baseline Acoustic Models for
> Brazilian Portuguese Using Kaldi Tools. Proc. IberSPEECH 2018, 77-81, DOI:
> 10.21437/IberSPEECH.2018-17.

```bibtex
@inproceedings{Batista2018,
  author    = {Cassio Batista and Ana Larissa Dias and Nelson {Sampaio Neto}},
  title     = {{Baseline Acoustic Models for Brazilian Portuguese Using Kaldi Tools}},
  year      = {2018},
  booktitle = {Proc. IberSPEECH 2018},
  pages     = {77--81},
  doi       = {10.21437/IberSPEECH.2018-17},
  url       = {http://dx.doi.org/10.21437/IberSPEECH.2018-17}
}
```    

[![FalaBrasil](doc/logo_fb_github_footer.png)](https://ufpafalabrasil.gitlab.io/ "Visite o site do Grupo FalaBrasil") [![UFPA](doc/logo_ufpa_github_footer.png)](https://portal.ufpa.br/ "Visite o site da UFPA")

__Grupo FalaBrasil (2019)__ - https://ufpafalabrasil.gitlab.io/      
__Universidade Federal do Pará (UFPA)__ - https://portal.ufpa.br/     
Larissa Dias - larissa.engcomp@gmail.com    
Cassio Batista - https://cassota.gitlab.io/
