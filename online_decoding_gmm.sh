#!/bin/bash
# Note: you have to do 'make ext' in $kaldi_path/src/ before running this.
#
# This script is meant to demonstrate how an existing HMM-GMM acoustic model (AM) can be used to decode new audio files.
#
# USAGE:
#    $ ./online_decoding.sh
# 
# Directory tree:
#    KALDI_ROOT/egs/YOUR_PROJECT_NAME   
#
#    AM/
#       tri3_8k_16/
#            final.mat
#            final.mdl
#            HCLG.fst
#            phones.txt
#            words.txt
#            
#    audio/
#       test1.wav
#       test2.wav
#       test3.wav
#
#
# OUTPUT:
#    decode_dir/
#        ali.txt
#        input.scp
#        trans.txt
#        transcriptions.txt

. ./cmd.sh
. ./path.sh
. ./utils/parse_options.sh

# Set the paths to the binaries and scripts needed
KALDI_ROOT=`pwd`/../..
export PATH=$PWD/utils/:$KALDI_ROOT/src/onlinebin:$KALDI_ROOT/src/online2bin:$KALDI_ROOT/src/bin:$KALDI_ROOT/tools/portaudio:$PATH


data_file="AM"


ac_model_type=tri3_8k_16


# Alignments and decoding results are saved in this directory
decode_dir="./online_decoding_gmm"

# Change this to "live" either here or using command line switch like:
# --test-mode live
test_mode="simulated"



# Set the path to the acoustic models
ac_model=${data_file}/$ac_model_type


# Set the path to the audio files
audio=audio

trans_matrix=""
if [ -s $ac_model/final.mat ]; then
    trans_matrix=$ac_model/final.mat
fi

case $test_mode in
    live)
        echo
        echo -e "  LIVE DEMO MODE - you can use a microphone and say something\n"
        echo
        online-gmm-decode-faster --rt-min=0.5 --rt-max=0.7 --max-active=4000 \
           --beam=12.0 --acoustic-scale=0.0769 $ac_model/model $ac_model/HCLG.fst \
           $ac_model/words.txt '1:2:3:4:5' $trans_matrix;;

    simulated)
        echo
        echo -e "  SIMULATED ONLINE DECODING - pre-recorded audio is used\n"
        echo
        echo "  You can type \"./run.sh --test-mode live\" to try it using your"
        echo "  own voice!"
        echo
        mkdir -p $decode_dir
        # make an input .scp file
        > $decode_dir/input.scp
        for f in $audio/*.wav; do
            bf=`basename $f`
            bf=${bf%.wav}
            echo $bf $f >> $decode_dir/input.scp
        done
        online-wav-gmm-decode-faster --verbose=1 --rt-min=0.8 --rt-max=0.85\
            --max-active=4000 --beam=12.0 --acoustic-scale=0.0769 \
            scp:$decode_dir/input.scp $ac_model/final.mdl $ac_model/HCLG.fst \
            $ac_model/words.txt '1:2:3:4:5' ark,t:$decode_dir/trans.txt \
            ark,t:$decode_dir/ali.txt $trans_matrix | tee -a $decode_dir/transcriptions.tmp;;


    *)
        echo "Invalid test mode! Should be either \"live\" or \"simulated\"!";
        exit 1;;
esac

# Saving transcriptions to a file
sed '/^[[:space:]]*$/d' $decode_dir/transcriptions.tmp > $decode_dir/transcript.tmp
awk -F" " '{print $1}' $decode_dir/input.scp  >> $decode_dir/filename.tmp
paste $decode_dir/filename.tmp $decode_dir/transcript.tmp > $decode_dir/transcriptions.txt

rm $decode_dir/*.tmp

