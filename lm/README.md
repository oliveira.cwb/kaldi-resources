# Language Model for Brazilian Portuguese Using SRILM

**[Paper](https://www.isca-speech.org/archive/IberSPEECH_2018/abstracts/IberS18_P1-13_Batista.html): Baseline Acoustic Models for Brazilian Portuguese Using Kaldi Tools**    
A comparison between Kaldi and CMU Sphinx for Brazilian Portuguese was
performed. Resources for both toolkits were developed and made publicly 
available to the community.

* __lm.arpa__:   A N-gram language model built with the
[SRILM](http://www.speech.sri.com/projects/srilm/download.html) toolkit. For
training, the [CETENFolha](https://www.linguateca.pt/cetenfolha/index_info.html)
corpora was used, consisting of 1.6 million sentences extracted from Folha de S.
Paulo Newspaper and compiled by the Center for Computational Linguistics of the
University of São Paulo ([NILC-USP](http://www.nilc.icmc.usp.br/nilc/index.php)). 
The dictionary used in training was UFPAdic3.0 with 64,972 words. The model is
perplexed 170 and the smoothing technique used was Witten-Bell discounting. 


## Citation

If you use these resources or want to mention the paper referred above, please 
cite us as one of the following: 

> Batista, C., Dias, A.L., Sampaio Neto, N. (2018) Baseline Acoustic Models for
> Brazilian Portuguese Using Kaldi Tools. Proc. IberSPEECH 2018, 77-81, DOI:
> 10.21437/IberSPEECH.2018-17.

```bibtex
@inproceedings{Batista2018,
  author    = {Cassio Batista and Ana Larissa Dias and Nelson {Sampaio Neto}},
  title     = {{Baseline Acoustic Models for Brazilian Portuguese Using Kaldi Tools}},
  year      = {2018},
  booktitle = {Proc. IberSPEECH 2018},
  pages     = {77--81},
  doi       = {10.21437/IberSPEECH.2018-17},
  url       = {http://dx.doi.org/10.21437/IberSPEECH.2018-17}
}
```    

[![FalaBrasil](doc/logo_fb_github_footer.png)](https://ufpafalabrasil.gitlab.io/ "Visite o site do Grupo FalaBrasil") [![UFPA](doc/logo_ufpa_github_footer.png)](https://portal.ufpa.br/ "Visite o site da UFPA")

__Grupo FalaBrasil (2019)__ - https://ufpafalabrasil.gitlab.io/      
__Universidade Federal do Pará (UFPA)__ - https://portal.ufpa.br/     
Larissa Dias - larissa.engcomp@gmail.com    
Cassio Batista - cassio.batista.13@gmail.com     
