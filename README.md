# ASR resources for Kaldi in PT\_BR

**[Paper](https://www.isca-speech.org/archive/IberSPEECH_2018/abstracts/IberS18_P1-13_Batista.html): Baseline Acoustic Models for Brazilian Portuguese Using Kaldi Tools**    
A comparison between Kaldi and CMU Sphinx for Brazilian Portuguese was
performed. Resources for both toolkits were developed and made publicly 
available to the community.

# Requirements:
* __Git__: is needed to download Kaldi and this recipe.    
* __Kaldi__: is the toolkit for speech recognition that we use. This software is available at https://github.com/kaldi-asr/kaldi.     
* __Portaudio__: is an open-Source audio API. You must install it using the script `install_portaudio.sh` located at `path/to/kaldi/tools/extras/` dir. **NOTE**: If you try to install Portaudio using another way, the online decoding scripts will not work.    

# Preparing directories:   

The directory tree for new projects must follow the structure below:

```
           path/to/kaldi/egs/YOUR_PROJECT_NAME/
                                 ├─ path.sh
                                 ├─ cmd.sh
                                 ├─ online_decoding_gmm.sh
                                 └─ online_decoding_dnn.sh                                      
```     

__IMPORTANT__: althought you might have compiled Kaldi sources, you also must 
execute `make ext` in `/path/to/kaldi/src/` before running the decoding 
scripts. This will generate the binaries under the `online*/` dirs.

* __online_decoding_gmm.sh__: This script is adapted from the Voxforge recipe for 
[online decoding](https://github.com/kaldi-asr/kaldi/tree/master/egs/voxforge/online_demo).
It is meant to demonstrate how an existing HMM-GMM can be used to decode new 
audio files.    
* __online_decoding_dnn.sh__: This script is meant to demonstrate how an 
existing DNN-HMM acoustic model trained with iVectors can be used to decode new 
audio files.    
* __audio/__: contains 3 audio files transcribed to work as examples.     
* __am/__: Acoustic models for Brazilian Portuguese using Kaldi tools.      
* __lm/lm.arpa__: A N-gram language model built with the [SRILM](http://www.speech.sri.com/projects/srilm/download.html) toolkit.

Scripts for online decoding can be execute as follows:

```bash
# GMM
$ ./online_decoding_gmm.sh # pre-recorded audio
$ ./online_decoding_gmm.sh --test-mode live # live your own voice via mic input
# DNN
$ ./online_decoding_dnn.sh # pre-recorded audio
```   


## Citation

If you use these resources or want to mention the paper referred above, please 
cite us as one of the following: 

> Batista, C., Dias, A.L., Sampaio Neto, N. (2018) Baseline Acoustic Models for
> Brazilian Portuguese Using Kaldi Tools. Proc. IberSPEECH 2018, 77-81, DOI:
> 10.21437/IberSPEECH.2018-17.

```bibtex
@inproceedings{Batista2018,
  author    = {Cassio Batista and Ana Larissa Dias and Nelson {Sampaio Neto}},
  title     = {{Baseline Acoustic Models for Brazilian Portuguese Using Kaldi Tools}},
  year      = {2018},
  booktitle = {Proc. IberSPEECH 2018},
  pages     = {77--81},
  doi       = {10.21437/IberSPEECH.2018-17},
  url       = {http://dx.doi.org/10.21437/IberSPEECH.2018-17}
}
```    

[![FalaBrasil](doc/logo_fb_github_footer.png)](https://ufpafalabrasil.gitlab.io/ "Visite o site do Grupo FalaBrasil") [![UFPA](doc/logo_ufpa_github_footer.png)](https://portal.ufpa.br/ "Visite o site da UFPA")

__Grupo FalaBrasil (2019)__ - https://ufpafalabrasil.gitlab.io/      
__Universidade Federal do Pará (UFPA)__ - https://portal.ufpa.br/     
Larissa Dias - larissa.engcomp@gmail.com    
Cassio Batista - https://cassota.gitlab.io/
